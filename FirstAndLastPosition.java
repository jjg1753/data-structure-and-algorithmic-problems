//Problem : 34: "Find First and Last Position of Element in Sorted Array", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)
//Given an array of integers nums sorted in ascending order,
//find the starting and ending position of a given target value.
//Algorithm runtime complexity: O(logn)

class FirstAndLastPosition {
  public static void main(String arguments[]){
    int[] nums = {2, 2, 3, 4, 5, 2};
    System.out.print(java.util.Arrays.toString(searchRange(nums, 2)));
  }
    public static int[] searchRange(int[] nums, int target) {
        int[] answer = {-1, -1};
        boolean check = true;
        for(int i = 0; i < nums.length; i++){
          if(check) {
          if(nums[i] == target) {
            answer[0] = i;
            check = false;
          }
        } else {
          if(nums[i] == target && i == nums.length - 1) {
            answer[1] = i;
            return(answer);
          }
          else if(nums[i] != target) {
            answer[1] = i - 1;
            return(answer);
          }
        }
        }

        if(answer[0] != -1) {
          answer[1] = answer[0];
          return(answer);
        }
        return(answer);
    }
}
