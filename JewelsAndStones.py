
"""
Problem 771: "Jewels and Stones", LeetCode
Author: Jeet Gandhi(gandhi.jeet@gmail.com)

You're given strings J representing the types of stones that are jewels,
and S representing the stones you have.  Each character in S is a type of
stone you have.  You want to know how many of the stones you have are also
jewels.
"""

def numJewelsInStones(J, S):
    """
    :type J: str
    :type S: str
    :rtype: int
    """
    stone_dict = {}

    for stone in S:
        if stone in stone_dict.keys():
            stone_dict[stone] = stone_dict[stone] + 1
        else:
            stone_dict[stone] = 1

    total = 0
    for jewel in J:
        if jewel in stone_dict.keys():
            total = total + stone_dict[jewel]

    print(total)

def main():
    numJewelsInStones("j", "JJ");

if __name__ == "__main__":
    main()
