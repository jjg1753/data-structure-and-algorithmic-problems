//Problem 283: "Move Zeroes", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)


var moveZeroes = function(nums) {
    var tempNumber = [];
    var tempZero = [];
    for (var item = 0; item < nums.length; item++){
      if(nums[item] == 0){
        //console.log(nums[item]);
          tempZero.push(nums[item]);
      }else{
        //console.log(nums[item]);
          tempNumber.push(nums[item]);
      }
}
    nums = tempNumber.concat(tempZero);
    //console.log(tempZero)
    //console.log(tempNumber);
    console.log(nums);

};
var nums = [0, 1, 0, 3, 12]
moveZeroes(nums);
