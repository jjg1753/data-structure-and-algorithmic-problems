#Given an array of integers, return indices of the two numbers such that they add up to a specific target.
"""
Problem 1: "Two Sum", LeetCode
Author: Jeet Gandhi(gandhi.jeet@gmail.com)
"""
def main():
     nums = [3,2,4]
     print(twoSum(nums, 6))

def twoSum(nums, target):
    hashmap = {}
    for idx, val in enumerate(nums):
        hashmap[val] = idx
    for idx, val in enumerate(nums):
        difference = target - val
        if difference in hashmap and hashmap[difference] != idx:
            return [hashmap[difference], idx]

if __name__ == "__main__":
    main()
