//Problem 38: "Count and Say", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)
//Given an integer n where 1 ≤ n ≤ 30, generate the nth term of the
//count-and-say sequence.

/**
 * @param {number} n
 * @return {string}
 */
var countAndSay = function(n) {
    var result = "";
    var firstElement = "1";
    if(n<=1)
      return firstElement;

    var j;
    for(var i = 2; i <= n; i++){
      for(j = 0; j < firstElement.length; j++){
        var count = 1;
        ts = firstElement.charAt(j);
        //While loop to count the number of occurences of each number
        while(j < firstElement.length - 1 && firstElement[j] == firstElement[j + 1]){
          count ++;
          j++;
        }
        result += count.toString();
        result += ts;
      }
      firstElement = result;
      result = "";
    }

};

countAndSay(3);
