//Problem 389: "Find the Difference", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)

/**
 * @param {string} s
 * @param {string} t
 * @return {character}
 */

 function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

var findTheDifference = function(s, t) {
  hashmapT = new Object();

  //Creating a Dictionary for all the characters in string t which has an extra
  //letter with key as the letter and value as the frequency of that letter
  for (var i = 0; i < t.length; i++){
    if(t[i] in hashmapT){
      hashmapT[t[i]] = hashmapT[t[i]] + 1;
    }else{
      hashmapT[t[i]] = 1;
    }
  }

//Using the shorter string to decrease the letter frequency based on their
//occurunces in the shorter string
for(var i = 0; i < s.length; i++){
  hashmapT[s[i]] = hashmapT[s[i]] - 1;
}

return(getKeyByValue(hashmapT, 1))

};

console.log(findTheDifference('abad', 'abada'));
