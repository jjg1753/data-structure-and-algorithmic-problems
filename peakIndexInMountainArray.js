/**
Problem 852: "Return the index of the peak in a mountain array", LeetCode
author: Jeet Gandhi(gandhi.jeet@gmail.com)
Given an array that is definitely a mountain, return any i such that
A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1].
*/

/**
 * @param {number[]} A
 * @return {number} index of the peak
 */
var peakIndexInMountainArray = function(A) {
  var left = 0;
  var right = A.length - 1;
  while (left < right) {
    var mid = left + Math.floor((right - left) / 2);
    if (A[mid] < A[mid + 1]) {
      left = mid + 1;
    }
    else {
      right = mid;
    }
    return left;
  }
};

var nums = [0, 1, 2, 0];
peakIndexInMountainArray(nums)
