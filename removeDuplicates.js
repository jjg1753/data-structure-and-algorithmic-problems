// Problem 26: "Remove Duplicates From Sorted Array", LeetCode
// author: Jeet Gandhi(gandhi.jeet@gmail.com)
// Given a sorted array nums, remove the duplicates in-place such that each
// element appear only once and return the new length.


/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    var tracked = nums[0];
    for (var idx = 1; idx < nums.length; idx++){
      if(nums[idx] == tracked) {
        nums.splice(idx, 1);
        idx--;
      } else {
        tracked = nums[idx];
      }
    }
};

var nums = [1, 1, 2, 2];
removeDuplicates(nums)
console.log(nums);
