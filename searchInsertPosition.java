//Problem : 35: "Seach Insert Position", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)
//Given a sorted array and a target value, return the index if the
//target is found. If not, return the index where it would be if it
//were inserted in order.

class searchInsertPosition {
  public static void main(String arguments[]){
    int[] nums = {1, 3, 5, 6};
    System.out.print(searchInsert(nums, 0));
  }
    public static int searchInsert(int[] nums, int target) {
        for(int i = 0; i < nums.length; i++) {
          if(nums[i] == target)
            return(i);
        else if(nums[i] > target)
            return(i);
        else if(nums[i] < target && i == nums.length - 1)
            return(i + 1);
        }
        return(0);

    }
}
