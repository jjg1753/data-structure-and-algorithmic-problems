//Problem 605: "Can Place Flowers", LeetCode
//author: Jeet Gandhi(gandhi.jeet@gmail.com)

/**
 * @param {number[]} flowerbed
 * @param {number} n
 * @return {boolean}
 */
var canPlaceFlowers = function(flowerbed, n) {
  var i = 0;
  var count = 0;
    while (i < flowerbed.length) {
            if (flowerbed[i] == 0 && (i == 0 || flowerbed[i - 1] == 0) && (i == flowerbed.length - 1 || flowerbed[i + 1] == 0)) {
                flowerbed[i] = 1;
                count++;
            }
            i++;
        }
        return count >= n;
};

console.log(canPlaceFlowers([1, 0, 0, 0, 1], 2))
